# jinglesound

Erstellen Sie einen neuen Jingle-Sound für ein Linux-Telefon

Wenn Sie es vorziehen, Ihren Lieblingssong zu hören, wenn Sie Anrufe erhalten, aufstehen oder andere Termine haben, werden Sie "jinglesound" lieben!

Starten Sie die App, stellen Sie den Typ ein, wählen Sie Ihre Lieblingsmelodie, fertig.

- Sie können die drei Hauptklänge jederzeit ändern
- der Sound kann zur Auswahl in der Vorschau angezeigt werden
- falls vorhanden, wird das Albumcover angezeigt
- nach der Umwandlung das gleiche
- wenn es Ihnen nicht gefällt, können Sie es erneut versuchen
- die Anzeige in Landessprache ist leicht anpassbar
- derzeit in englisch und deutsch

Laden Sie zuerst das Archiv herunter und entpacken Sie es dann. Dann können Sie die Standardoptionen im Header des Skripts anpassen oder die Installation direkt in einem Terminal starten:

<code>./jinglesound install now  # als root oder via sudo</code>

Während der Installation werden Ihnen einige Fragen gestellt, bitte antworten Sie mit "yes", wenn Sie damit einverstanden sind, ansonsten bricht die Installation ab oder es wird nur das Standard-Icon für die App verwendet. Mein Icon-Favorit wird angezeigt und als App-Icon verwendet, wenn es Ihnen auch gefällt.

Jinglesound benötigt einige Programme, die Sie möglicherweise noch nicht haben. Die fehlenden Pakete werden vor der Installation anzeigt. Nach Ihrer Zustimmung werden diese heruntergeladen sowie installiert. Ihr Linux-Telefon sollte daher aufgeladen oder an eine Stromquelle angeschlossen sein.

Falls bei der Installation von jinglesound Fehler auftreten, werden diese angezeigt. Wenn Sie die Ursachen dafür beseitigt haben, können Sie die Installation unter Angabe der Fehlernummer fortsetzen:

<code>./jinglesound install "error-code"  # als root oder via sudo</code>

# Update auf Version 0.3.1 - mit Vorschaubild und Deinstallation

Die Installation macht alles in einem Rutsch und man kann sie dreimal abbrechen:
1. nach dem Start
2. falls Programme fehlen - vor dem Systemupdate
3. wenn alle Abhängigkeiten erfüllt sind - vor der App-Installation.

Es wird auch eine Log-Datei erstellt!

Also viel Spaß beim Ausprobieren und wem die "Installationshilfe" nicht gefällt, der kann es auch zu Fuß machen:
Einfach die nur Datei "jinglesound" herunterladen und dann die Links, *.desktop und die 16-Icons selbst erstellen.
Hinweise dazu finden sich in der "uninstall". 

Fragen oder Feedback zu "jinglesound" an: [#jinglesound:matrix.org](https://matrix.to/#/#jinglesound:matrix.org)

Viel Spaß beim Ausprobieren und eine gute Zeit wünscht

mo2mo
