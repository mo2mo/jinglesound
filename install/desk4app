#!/bin/bash

# The script "desk4app" make appName.desktop for a app.
# version: 0.3.2 - Oct 11, 2021

# For further information:
# https://gitlab.com/mo2mo/jinglesound
 
# dependencies:
# s. PROGS in appName

# Author:
by="@mo2mo:matrix.org"   # Script released under GPLv3
# https://www.gnu.org/licenses/gpl-3.0-standalone.html

# License GPLv2+: GNU GPL version 2 or later <http://gnu.org/licenses/gpl.html>
# This is free software: you are free to change and redistribute it.
# There is NO WARRANTY, to the extent permitted by law."

##---------- Adjust if necessary ------------------------------------------------------------
##-------------------------------------------------------------------------------------------

# local language
LNG="de en"

# input for appName.desktop
VAR="cmnt gnrc name"

# read from app2install!
# user and group
[ -z "$GROUP" ] && GROUP="users"; usrID=1000
[ -z "$USERS" ] && USERS=$(grep "$usrID" /etc/group | cut -d: -f1)

# folder for the appName.desktop
[ -z "$desktAPP" ] && desktAPP="/usr/share/applications"

# Apps come in the folder /usr/local/bin or in home = ~/.bin from $USERS:
[ -z "$appfolder" ] && appfolder="/home/$USERS/.bin"
# app pictures are there: ~/.bin/icons
# Don't forget to adjust the path variable or set a link from /usr/local/bin to $appfolder!

##----- End of the settings area ------------------------------------------------------------
##===========================================================================================

# Display of the info in the own local language --> en = *)
# LNG=$(echo $LANG | cut -d_ -f1)
function infos() {
  case "$2" in
      de) case "$1" in
            cmnt)  cmntDE="Erstellt einen neuen Jingelsound" ;;  # Comment
            gnrc)  gnrcDE="Ändert einen Jingelsound"         ;;  # GenericName
            name)  nameDE="$NAMES"                           ;;  # APP-Name
          esac  ;;
       *) case "$1" in
            cmnt)  cmntEN="Creates a new jingle sound"       ;;  # Comment
            gnrc)  gnrcEN="Choose jingle sound"              ;;  # GenericName
            name)  nameEN="$NAMES"                           ;;  # app name
          esac  ;;
  esac
}

##--------------------------------------------------------------------------------------------------

function install4desk() {
  [ ! -f "$appfolder/$1" ] && cp "$1" "$appfolder/$1"
  chmod +x $appfolder/$1
  [ $? -ne 0 ] && exit 22
  for i in $VAR; do
    for j in $LNG; do infos $i $j; done
  done
  cat <<EOF > "$DSKTP"
[Desktop Entry]
Name=$nameEN
Name[de]=$nameDE
GenericName=$gnrcEN
GenericName[de]=$gnrcDE
Comment=$cmntEN
Comment[de]=$cmntDE
Icon=preferences-system-notifications
Exec=$1 $2
Type=Application
StartupNotify=true
Terminal=false
EOF
  if [ -f "$DSKTP" ]
  then cp "$DSKTP" "$appfolder/${DSKTP##*/}"
       chown $USERS:$GROUP "$appfolder/${DSKTP##*/}"
       ERR=30
  else ERR=23
  fi
  exit $ERR
}

##--------------------------------------------------------------------------------------------------
# main
# call from the script:
# app2install "$NAMES" "$OPT"

[ ! -z "$1" ] &&  NAMES="$1" || exit 21           # App for install
[ ! -z "$2" ] || [ "$2" != "[OPT]" ]  && OPT="$2" # [option(s)] for app
[ -z "$DSKTP" ] && DSKTP="$desktAPP/$NAMES.desktop"

# echo -e "desk4app - $1, \$2=\$OPT: \"$2=$OPT\""
ERR=0
install4desk "$NAMES" "$OPT"
[ $ERR -eq 0 ] && exit 30 || exit $ERR
