#!/bin/bash

# The script "link4app" make links to file(s) for a app.
# version: 0.3.2 - Oct 11, 2021

# For further information:
# https://gitlab.com/mo2mo/jinglesound

# dependencies:
# s. PROGS in appName

# Author:
by="@mo2mo:matrix.org"   # Script released under GPLv3
# https://www.gnu.org/licenses/gpl-3.0-standalone.html

# License GPLv2+: GNU GPL version 2 or later <http://gnu.org/licenses/gpl.html>
# This is free software: you are free to change and redistribute it.
# There is NO WARRANTY, to the extent permitted by law."


##---------- Adjust if necessary ------------------------------------------------------------
##-------------------------------------------------------------------------------------------

objART="oga"                 # object art
object="phone-incoming-call" # object to change
source="/usr/share/sounds"   # object folder
target=".local/share/sounds" # object folder in user home
themes="freedesktop"         # under folder in object folder
thmART="stereo"              # under folder in theme
tmpSUM="/tmp/objects.sum"    # file for hash values of object

# read from app2install!
# user and group
[ -z "$GROUP" ] && GROUP="users"; usrID=1000
[ -z "$USERS" ] && USERS=$(grep "$usrID" /etc/group | cut -d: -f1)

# Apps come in the folder /usr/local/bin or in home = ~/.bin from $USERS:
[ -z "$appfolder" ] && appfolder="/home/$USERS/.bin"
# app pictures are there: ~/.bin/icons
# Don't forget to adjust the path variable or set a link from /usr/local/bin to $appfolder!

##----- End of the settings area ------------------------------------------------------------
##===========================================================================================

# make_link "$source" "$object" "$target"
function make_link() {
  for i in $2; do
    LINK=$(readlink "$1/$i.$EC")
    if [ -z "$LINK" ]
    then [ -z "$orgFile" ] && orgFile="$i" || orgFile="$orgFile $i"
         mv "$1/$i.$EC" "$1/$i.$EC.save"
         [ $? -eq 0 ] && ERR=0 || exit 34
         cp "$1/$i.$EC.save" "$3/$i.$EC"
         [ $? -eq 0 ] && ERR=0 || exit 35
    else echo " LINK = $LINK"
    fi
    [ "$LINK" != "$3/$i.$EC" ] && ln -s "$3/$i.$EC" "$1/$i.$EC"
    chown $USERS:$GROUP "$1/$i.$EC"
  done
}

# save_hash "$orgFile" "$object"
function save_hash() {
  if [ ! -z "$1" ] && [ "$1" = "$2" ]
  then for i in $1; do sha256sum $source/$i.oga.save; done > "$tmpSUM"
       if [ -f "$TAR" ] && [ -f "$tmpSUM" ]
# planned, still in work: compare hash values of sound files 
      then 
#            gunzip "$TAR"
#            tar -vrf *.tar "$tmpSUM" >/dev/null 2>&1
            cat "$tmpSUM" >> "$log"
#            gzip "${TAR%.gz*}"
            rm "$tmpSUM"
       else echo "No archive found!"
       fi
  else echo -e "error in $(basename "$0") in save_hash:\
              \n$orgFile\n$object"
  fi
}

##-------------------------------------------------------------------------------------------
## main
#  call from scrip app2install:
#  link4app links "$object" "$target" $themes

[ -z "$1" ] || [ ! "$1" = links ] && exit 31
[ ! -z "$2" ] && object="$2"
[ ! -z "$3" ] && target="$3"
[ ! -z "$4" ] && themes="$4"

source="$source/$themes/$thmART"  # object source
target="/home/$USERS/$target"     # object target in user home

[ ! -d "$source" ] && exit 32
[ ! -d "$target" ] && mkdir "$target" && chown $USERS:$GROUP "$target"
[ ! -d "$target" ] && exit 33

ERR=0 ; EC="$objART"

make_link "$source" "$object" "$target"
save_hash "$orgFile" "$object"

[ $ERR -eq 0 ] && exit 40 || exit $ERR
