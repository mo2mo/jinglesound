# jinglesound

Create a new jingle sound for a Linux phone

If you prefer to hear your favorite song when you receive calls, get up or have other appointments, you will love "jinglesound"!

Launch the app, set the type, choose your favorite tune, done.

- You can change the three main sounds at any time
- the sound can be previewed for selection
- if available, the album cover will be displayed
- after the conversion the same
- if you don't like it, you can try again
- the display in local language is easily customizable
- currently in english and german

first download the archive and then unzip it. Then you can adjust the default options in the header of the script or start the installation directly in a terminal:

<code>./jinglesound install now  # as root or via sudo</code>

During the installation you will be asked some questions, please answer "yes" if you agree, otherwise the installation will abort or only the default icon for the app will be used. My icon favovrit will be displayed and used as app icon if you like it too.

Jinglesound requires some programs that you may not already have. The missing packages will be displayed before installation. After your approval, they will be downloaded as well as installed. Your Linux phone should therefore be charged or connected to a power source.

If errors occur during the installation of jinglesound, they will be displayed. Once you have eliminated the causes of them, you can continue the installation by specifying the error number:

<code>./jinglesound install "error-code"  # as root or via sudo</code>

# Update to version 0.3.1 - with preview image and uninstall.

The installation does everything in one go and you can cancel it three times:
1. after the start
2. if programs are missing - before system update
3. if all dependencies are fulfilled - before app installation.

It will also create a log file!

So have fun trying it out and if you don't like the "installation help" you can do it on foot:
Just download the jinglesound and create the links, *.desktop and the 16-icons yourself. Hints for this can be found in the "uninstall". 

Questions or feedback about "jinglesound" to: [#jinglesound:matrix.org](https://matrix.to/#/#jinglesound:matrix.org)

Have fun trying out a good time wishes

mo2mo
